# README #

### Installation and run the test ###

* Install Jdk 1.7, set JAVA_HOME
* Install Maven 3, set MAVEN_HOME
* CD to the folder where to store pom.xml file
* Run command: mvn -Dtest=<Class Name>#<Method Name> test
* Class Name = JiraTest
* Method Name = testCreateIssue
* Method Name = testEditIssue
* Method Name = testSearchIssue
* Example:
* mvn  -Dtest=JiraTest#testCreateIssue test
* mvn  -Dtest=JiraTest#testEditIssue test
* mvn  -Dtest=JiraTest#testSearchIssue test
* Can be run using Eclipse with Maven

### Source code structure ###

### Source file: ###
AbstractPage.java
HomePage.java
LoginPage.java
CreateIssuePage.java
EditIssuePage.java
SearchIssuePage.java
### Test file: ###
TestJira.java
testCreateIssue
testEditIssue
testSearchIssue
### Util ###
* ExerciseConstants.java: stores all constants for input
* ExerciseUtils.java
### Resource: ###
* config.properties: to configure the browser
* testng-customsuite.xml to configure how the test run 
executed. This follows TESTNG rules.

### Test Scenarios ###
### TEST 1: testCreateIssue ###
* Precondition: Login to Jira
* Step 1: Create a new issue with:
* Issue Type
* Priority
* Summary
* Environment
* Description
* Validation point: Verify a successful popup message is displayed

### TEST 2: testEditIssue ###
* Precondition: Login to Jira
* Step 1: Navigate to the existing issue
* Step 2: Edit some information of the issue
* Issue Type
* Priority
* Summary
* Validation point: Verify that all edited information are correct

### TEST 3: testSearchIssue ###
* Precondition: Login to Jira
* Step 1: Perform a search for an existing issue
* Validation point: Verify that the issue can be found