package com.atlassian.exercise.util;


public class ExerciseConstants {
	
	public static final String BROWSER_URL = "https://jira.atlassian.com/browse/TST";
	public static final String TIME_OUT_KEY = "time_out";
	public static final String BROWSER_DRIVER_CLASS_KEY = "browser_class";
	
	public static final String LOGIN_LINK_TEXT = "Log In";
	public static final String USERNAME_TEXT_ID = "username";
	public static final String LOGIN_BUTTON_ID = "login-submit";
	public static final String REMEMBER_ME_CHECKBOX_ID = "rememberMe";
	public static final String PASSWORD_TEXT_ID = "password";
	public static final String USERNAME = "quyenzip@gmail.com";
	public static final String PASSWORD = "Trial@288";
	
	public static final String QUICK_SEARCH_LOCATOR = "quickSearchInput";
	public static final String ISSUE_ID_TEST = "TST-57245";
	public static final String EDIT_ISSUE_BROWSER_URL = "https://jira.atlassian.com/browse/TST-57245";
	public static final String ISSUE_TYPE_BUG = "Bug";
	public static final String ISSUE_TYPE_NEWFEATURE = "New Feature";
	
	public static final String PRIORITY_MAJOR = "Major";
	public static final String PRIORITY_MINOR = "Minor";
	
	public static final String CREATE_ISSUE_LINK_ID = "create_link";
	public static final String ISSUE_SUMMARY_TEXT = "[Test] Cannot create new issue on Jira";
	public static final String ISSUE_SUMMARY_TEXT_EDITED = "Failed to edit an existing issue on Jira";
	
	public static final String PROJECT_LOCATOR = "";
	public static final String SUMMARY_LOCATOR = "summary";
	public static final String SECURITY_LEVEL_LOCATOR = "security";
	public static final String PRIORITY_LOCATOR = "Critical";
	public static final String COMPONENT_LOCATOR = "";
	public static final String AFFECT_VERSION_LOCATOR = "";
	public static final String FIX_VERSION_LOCATOR = "";
	public static final String ASSIGNEE_LOCATOR = "";
	public static final String ENVIRONMENT_LOCATOR = "environment";
	public static final String ENVIRONMENT_TEXT= "Windows 7";
	public static final String DESCRIPTION_LOCATOR = "description";
	public static final String DESCRIPTION_TEXT = "Steps to reproduce:\n- Step 1\n- Step 2";
	public static final String CREATE_LOCATOR = "";
	public static final String CANCEL_LOCATOR = "";
	public static final String DUE_DATE_LOCATOR = "";
	
}

