package com.atlassian.exercise.util;

import org.testng.Reporter;

public class ExerciseUtils {

	public static Object createObject(String className) {
		Object object = null;
		try {
			Class<?> classDefinition = Class.forName(className);
			object = classDefinition.newInstance();
		} catch (InstantiationException e) {
			Reporter.log(e.getMessage(), true);
		} catch (IllegalAccessException e) {
			Reporter.log(e.getMessage(), true);
		} catch (ClassNotFoundException e) {
			Reporter.log(e.getMessage(), true);
		}

		return object;
	}
}
