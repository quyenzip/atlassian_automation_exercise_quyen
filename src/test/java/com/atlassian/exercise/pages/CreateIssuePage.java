package com.atlassian.exercise.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

public class CreateIssuePage extends AbstractPage<CreateIssuePage> {
// Where to create a new issue on Jira Page. You can set the summary, issue type, priority and other information.
// Validation point: Verify if a popup message is displayed that the issue is created successfully.
	
	private By summaryLocator = By.id("summary");
	private By priorityLocator = By.id("priority-field");
	private By envLocator = By.id("environment");
	private By issueIdLinkLocator = By.xpath("//div[@class='global-msg']/div[@class='aui-message success closeable']");	
	private By descriptionLocator = By.id("description");
	private By submitButtonLocator = By.id("create-issue-submit");

	public CreateIssuePage(WebDriver driver) {
		super(driver);
	}
	
	/**
	 * Returns an CreateIssuePage object. 
	 * The issueType can be defined in ExerciseConstants.java.
	 * There are 6 issue types: Bug, New Feature, Task, Improvement, Third-party issue, Support request. 
	 * <p>
	 * This method to choose the issue type when creating a new issue. 
	 *
	 * @param  issueType  is a string which specifies the issue type.
	 * @return CreateIssuePage
	 */

	public CreateIssuePage chooseIssueType(String issueType) {
		Reporter.log("Choose issue type is bug </br>", true);
		getDriver().findElement(By.xpath("//div[@id='project-single-select']/span")).click();
		getDriver().findElement(By.xpath("//div[@id='project-single-select']/span")).click();
		getDriver().findElement(By.xpath("//div[@id='issuetype-single-select']/span")).click();
		getDriver().findElement(By.linkText(issueType)).click();

		return this;
	}

	/**
	 * Returns an CreateIssuePage object. 
	 * The summaryText can be defined in ExerciseConstants.java.
	 *  
	 * <p>
	 * This method to enter the summary when creating a new issue. 
	 *
	 * @param  summaryText  is a string which describes the issue summary.
	 * @return CreateIssuePage
	 */
	public CreateIssuePage enterIssueSummary(String summaryText) {
		Reporter.log("Enter bug summary </br>", true);

		getDriver().findElement(summaryLocator).clear();
		getDriver().findElement(summaryLocator).sendKeys(summaryText);

		return this;
	}

	/**
	 * Returns an CreateIssuePage object. 
	 * The priority can be defined in ExerciseConstants.java.
	 * There are 5 issue priorities: Trivial, Minor, Major, Critical, Blocker. 
	 * <p>
	 * This method to choose the priority when creating a new issue. 
	 *
	 * @param  priority  is a string which specifies the issue priority.
	 * @return CreateIssuePage
	 */
	public CreateIssuePage setPriority(String priority) {

		Reporter.log("Set bug priority </br>", true);
		new Select(getDriver().findElement(By.id("security"))).selectByVisibleText("Reporter and developers");
		getDriver().findElement(priorityLocator).sendKeys(priority);

		return this;
	}

	/**
	 * Returns an CreateIssuePage object. 
	 * The environment info can be defined in ExerciseConstants.java.
	 *  
	 * <p>
	 * This method to enter the environment info when creating a new issue. 
	 *
	 * @param  text  is a string which is entered as environment information.
	 * @return CreateIssuePage
	 */
	public CreateIssuePage enterEnviromentText(String text) {

		Reporter.log("Enter environment text </br>", true);
		getDriver().findElement(envLocator).clear();
		getDriver().findElement(envLocator).sendKeys(text);

		return this;
	}

	/**
	 * Returns an CreateIssuePage object. 
	 * The issue description can be defined in ExerciseConstants.java.
	 *  
	 * <p>
	 * This method to enter the issue description when creating a new issue. 
	 *
	 * @param  text  is a string which is entered as issue description.
	 * @return CreateIssuePage
	 */
	public CreateIssuePage enterDescriptionText(String text) {

		Reporter.log("Enter description text </br>", true);
		getDriver().findElement(descriptionLocator).clear();
		getDriver().findElement(descriptionLocator).sendKeys(text);

		return this;
	}

	/**
	 * Returns an CreateIssuePage object.
	 *  
	 * <p>
	 * This method to click on submit button. A new issue will be created. 
	 *
	 * @param
	 * @return CreateIssuePage
	 */
	public CreateIssuePage submitCreateIssue() {

		Reporter.log("Submit issue </br>", true);
		getDriver().findElement(submitButtonLocator).click();

		return this;
	}

	/**
	 * Returns an CreateIssuePage object. 
	 * The issueType, summary, priority, envText, description
	 * can be defined in ExerciseConstants.java.
	 * <p>
	 * This method to create a new issue
	 *
	 * @param  issueType  is a string which specifies the issue type.
	 * @param  summary  is a string which specifies the issue type.
	 * @param  priority is a string which specifies the issue priority.
	 * @param  envText is a string which is entered as environment information.
	 * @param  description is a string which is entered as issue description.
	 * @return CreateIssuePage
	 */
	public CreateIssuePage createIssue(String issueType, String summary, String priority, String envText, String description){
		chooseIssueType(issueType).waitSeconds(3000)//
		.enterIssueSummary(summary).waitSeconds(1000)//
		.setPriority(priority).enterEnviromentText(envText)//
		.enterDescriptionText(description)//
		.submitCreateIssue()
		.waitSeconds(5000);
		
		return this;
	}
	
	/**
	 * Returns true if a pop-up message is displayed. 
	 * The message specifies that a new issue is created successfully.
	 * 
	 * <p>
	 * This method to verify that a new issue is created successfully
	 * The method writes the PASSED/FAILED result into /target/surefire-reports/html
	 * 
	 * @param
	 * @return CreateIssuePage
	 */
	public boolean isMessageIssueCreatedSuccessfullyDisplayed() {
	
		if (getDriver().findElement(issueIdLinkLocator).isDisplayed())
		{
			Reporter.log("--- PASSED </br>", true);
			return true;
		}
		Reporter.log("--- FAILED </br>", true);
		return false;
	}

	/**
	 * Returns the issue ID when a new issue created. 
	 * Issue ID in String format.
	 * 
	 * <p>
	 * This method to get the issue ID when a new issue is created successfully
	 * 
	 * @param
	 * @return String
	 */
	public String getCreatedIssueID() {

		Reporter.log("Get the ID after the issue is created successfully </br>", true);
		String issueID = getDriver().findElement(issueIdLinkLocator).getText();
		Reporter.log(issueID, true);
		
		return issueID;
	}
	
	/**
	 * Returns CreateIssuePage object. 
	 * Issue ID in String format.
	 * 
	 * <p>
	 * This method to go into detail page a new issue is created successfully
	 * 
	 * @param
	 * @return CreateIssuePage
	 */
	public CreateIssuePage enterDetailCreatedIssue() {

		Reporter.log("Get the ID after the issue is created successfully </br>", true);
		getDriver().findElement(issueIdLinkLocator).click();
		
		return this;
	}

	@Override
	public CreateIssuePage getPage() {
		return this;
	}

}