package com.atlassian.exercise.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.atlassian.exercise.util.ExerciseConstants;

public class LoginPage extends AbstractPage<LoginPage> {

	// The login page contains several HTML elements that will be represented as
	// WebElements.
	// The locators for these elements should only be defined once.
	private By usernameLocator = By.id(ExerciseConstants.USERNAME_TEXT_ID);
	private By passwordLocator = By.id(ExerciseConstants.PASSWORD_TEXT_ID);
	private By loginButtonLocator = By.id(ExerciseConstants.LOGIN_BUTTON_ID);
	private By rememberLocator = By.id(ExerciseConstants.REMEMBER_ME_CHECKBOX_ID);

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	// The login page allows the user to type their username into the username
	// field
	/**
	 * Returns an LoginPage object. 
	 * username can be configured in ExerciseConstants.java
	 * <p>
	 * This method to type the username. 
	 *
	 * @param  username  is a string which specifies the JIRA username.
	 * @return LoginPage
	 */
	public LoginPage typeUsername(String username) {
		// This is the only place that "knows" how to enter a username
		getDriver().findElement(usernameLocator).sendKeys(username);

		// Return the current page object as this action doesn't navigate to a
		// page represented by another PageObject
		return this;
	}

	// The login page allows the user to type their password into the password
	// field
	/**
	 * Returns an LoginPage object. 
	 * password can be configured in ExerciseConstants.java
	 * <p>
	 * This method to type the password. 
	 *
	 * @param  password  is a string which specifies the JIRA password.
	 * @return LoginPage
	 */
	public LoginPage typePassword(String password) {
		// This is the only place that "knows" how to enter a password
		getDriver().findElement(passwordLocator).sendKeys(password);

		// Return the current page object as this action doesn't navigate to a
		// page represented by another PageObject
		return this;
	}

	/**
	 * Returns an LoginPage object. 
	 * 
	 * <p>
	 * This method to check/uncheck the option to remember account credentials 
	 *
	 * @param  remember  is a boolean value
	 * @return LoginPage
	 */
	public LoginPage checkRememberMe(Boolean remember) {
		// This is the only place that check or uncheck the option to remember
		// the user credentials.
		if (remember == false) {
			getDriver().findElement(rememberLocator).click();
		}

		return this;
	}

	/**
	 * Returns an HomePage object. 
	 * 
	 * <p>
	 * This method to submit the login 
	 *
	 * @param  
	 * @return HomePage
	 */
	// The login page allows the user to submit the login form
	public HomePage submitLogin() {
		// This is the only place that submits the login form and expects the
		// destination to be the home page.
		// A seperate method should be created for the instance of clicking
		// login whilst expecting a login failure.
		getDriver().findElement(loginButtonLocator).submit();

		// Return a new page object representing the destination. Should the
		// login page ever
		// go somewhere else (for example, a legal disclaimer) then changing the
		// method signature
		// for this method will mean that all tests that rely on this behaviour
		// won't compile.
		return new HomePage(getDriver());
	}

	// The login page allows the user to submit the login form knowing that an
	// invalid username and / or password were entered
	public LoginPage submitLoginExpectingFailure() {
		// This is the only place that submits the login form and expects the
		// destination to be the login page due to login failure.
		getDriver().findElement(loginButtonLocator).submit();

		// Return a new page object representing the destination. Should the
		// user ever be navigated to the home page after submiting a login with
		// credentials
		// expected to fail login, the script will fail when it attempts to
		// instantiate the LoginPage PageObject.
		return new LoginPage(getDriver());
	}

	// Conceptually, the login page offers the user the service of being able to
	// "log into"
	// the application using a user name and password.
	public HomePage loginAs(String username, String password, boolean isRememberMe) {
		// The PageObject methods that enter username, password & submit login
		// have already defined and should not be repeated here.
		typeUsername(username);
		typePassword(password);
		checkRememberMe(isRememberMe);
		return submitLogin();
	}

	@Override
	public LoginPage getPage() {
		return this;
	}
}