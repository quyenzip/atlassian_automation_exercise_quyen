package com.atlassian.exercise.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.atlassian.exercise.util.ExerciseConstants;

public class HomePage extends AbstractPage<HomePage> {

	public HomePage(WebDriver driver) {
		super(driver); 
	}

	/**
	 * Returns an HomePage object. 
	 * The browserUrl can be defined in ExerciseConstants.java.
	 * 
	 * <p>
	 * This method to navigate to the testing site. 
	 *
	 * @param  browserUrl  is a string which specifies the testing URL.
	 * @return HomePage
	 */
	public HomePage loadPage(String browserUrl){
		getDriver().get(browserUrl);
		
		return this;
	}

	/**
	 * Returns an LoginPage object. 
	 *  
	 * <p>
	 * This method to navigate to login page. 
	 *
	 * @param  
	 * @return LoginPage
	 */	
	public LoginPage gotoLogin(){
		Reporter.log("Login to JIRA </br>", true);
		getDriver().findElement(By.linkText(ExerciseConstants.LOGIN_LINK_TEXT)).click();
		return new LoginPage(getDriver());
	}

	/**
	 * Returns a CreateIssuePage object. 
	 *  
	 * <p>
	 * This method to navigate to create issue page. 
	 *
	 * @param  
	 * @return CreateIssuePage
	 */	
	public CreateIssuePage gotoCreateNewIssue(){
		Reporter.log("Create new issue on JIRA </br>", true);
		getDriver().findElement(By.id(ExerciseConstants.CREATE_ISSUE_LINK_ID)).click();
		
		return new CreateIssuePage(getDriver()) ;
	}
	
	/**
	 * Returns an EditIssuePage object. 
	 *  
	 * <p>
	 * This method to navigate to edit issue page. 
	 *
	 * @param  
	 * @return EditIssuePage
	 */	
	public EditIssuePage gotoEditIssue(){
		Reporter.log("Edit an existing issue on JIRA </br>", true);
				
		return new EditIssuePage(getDriver()) ;
	}
	
	/**
	 * Returns a SearchIssuePage object. 
	 *  
	 * <p>
	 * This method to navigate to Search Issue page. 
	 *
	 * @param  
	 * @return SearchIssuePage
	 */	
	public SearchIssuePage gotoSearchIssue(){
		Reporter.log("Search an existing issue on JIRA </br>", true);
		
		return new SearchIssuePage(getDriver());
	}

	@Override
	public HomePage getPage() {
		return this;
	}
}
