package com.atlassian.exercise.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.atlassian.exercise.util.ExerciseConstants;

public class SearchIssuePage extends AbstractPage<SearchIssuePage>{
	// Where to search an existing issue on Jira Page.
	// Validation point: Verify if the existing issue can be found on Jira search.
	
	public SearchIssuePage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Returns an SearchIssuePage object. 
	 * Issue ID in String format. Issue ID can be defined in ExerciseConstants.java.
	 * <p>
	 * This method to search an existing issue by JIRA search. 
	 *
	 * @param  issueID  is a string which specifies the issue ID.
	 * @return SearchIssuePage
	 */
	public SearchIssuePage searchExistingIssue(String issueID) {

		Reporter.log("Search an existing issue </br>", true);
		getDriver().findElement(By.id(ExerciseConstants.QUICK_SEARCH_LOCATOR)).clear();
		getDriver().findElement(By.id(ExerciseConstants.QUICK_SEARCH_LOCATOR)).sendKeys(issueID);
		getDriver().findElement(By.id(ExerciseConstants.QUICK_SEARCH_LOCATOR)).sendKeys(Keys.ENTER);
		
		return this;
	}
	
	/**
	 * Returns true if the existing issue can be found on JIRA search correctly.
	 * <p>
	 * This method to verify that the existing issue can be found on JIRA search correctly. 
	 * The method writes the PASSED/FAILED result into /target/surefire-reports/html
	 *
	 * @param  issueID  is a string which specifies the issue ID.
	 * @return Boolean
	 */
	public Boolean verifySearchResult(String issueID) {

		Reporter.log("Verify if the existing issue can be search using JIRA search </br>", true);
		searchExistingIssue(issueID);
		if (getDriver().getTitle().contains(issueID))
		{
			Reporter.log("--- PASSED </br>", true);
			return true;
		}
		Reporter.log("--- FAILED </br>", true);
		return false;
	}
	
	@Override
	public SearchIssuePage getPage() {
		return this;
	}

}
