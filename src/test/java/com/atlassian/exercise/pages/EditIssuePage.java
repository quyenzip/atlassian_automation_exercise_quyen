package com.atlassian.exercise.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

public class EditIssuePage extends AbstractPage<EditIssuePage> {
	// Where to edit an existing issue on Jira Page. You can edit the summary, issue type, priority and other information.
	// Validation point: Verify if the edited fields are updated correctly.

	private By issueUpdateButtonLocator = By.id("edit-issue-submit");
	private By issueFieldLocator = By.id("issuetype-field");
	private By priorityFieldLocator = By.id("priority-field");
	private By summaryLocator = By.id("summary");
	
	public EditIssuePage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Returns an EditIssuePage object. 
	 * The issueType can be defined in ExerciseConstants.java.
	 * There are 6 issue types: Bug, New Feature, Task, Improvement, Third-party issue, Support request. 
	 * <p>
	 * This method to edit the issue type. 
	 *
	 * @param  issueType  is a string which specifies the issue type.
	 * @return EditIssuePage
	 */
	public EditIssuePage editIssueType(String issueType) {
		Reporter.log("Edit the issue type </br>", true);
		getDriver().findElement(By.xpath("//span[@id='type-val']/span")).click();
		getDriver().findElement(issueFieldLocator).click();
		getDriver().findElement(issueFieldLocator).click();
		getDriver().findElement(issueFieldLocator).clear();
		getDriver().findElement(issueFieldLocator).sendKeys(issueType);
		getDriver().findElement(By.cssSelector("button.aui-button.submit")).click();

		return this;
	}

	/**
	 * Returns a String which specifies the issue type. 
	 * There are 6 issue types: Bug, New Feature, Task, Improvement, Third-party issue, Support request. 
	 * <p>
	 * This method to get the issue type. 
	 *
	 * @param  
	 * @return String
	 */
	public String getIssueType() {
		Reporter.log("Get the issue type </br>", true);
		return getDriver().findElement(By.xpath("//span[@id='type-val']")).getText();

	}

	/**
	 * Returns true if the issue type is edited correctly. 
	 * The issueType can be defined in ExerciseConstants.java.
	 * There are 6 issue types: Bug, New Feature, Task, Improvement, Third-party issue, Support request. 
	 * <p>
	 * This method to verify that the issue type is edited correctly. 
	 * The method writes the PASSED/FAILED result into /target/surefire-reports/html
	 *
	 * @param  expectedIssueType  is a string which specifies the issue type.
	 * @return Boolean
	 */
	public Boolean verifyIssueType(String expectedIssueType) {
		Reporter.log("Verify if the current issue type equals as expected issue type </br>", true);
		if (getIssueType().contains(expectedIssueType))
		{
			Reporter.log("--- PASSED </br>", true);
			return true;
		}
		Reporter.log("--- FAILED </br>", true);
		return false;
	}

	/**
	 * Returns an EditIssuePage object. 
	 * The priority can be defined in ExerciseConstants.java.
	 * There are 5 issue priorities: Trivial, Minor, Major, Critical, Blocker. 
	 * <p>
	 * This method to edit the issue priority. 
	 *
	 * @param  priority  is a string which specifies the issue priority.
	 * @return EditIssuePage
	 */
	public EditIssuePage editPriority(String priority) {
		Reporter.log("Edit the issue priority </br>", true);
		getDriver().findElement(By.xpath("//span[@id='priority-val']/span")).click();
		getDriver().findElement(priorityFieldLocator).click();
		getDriver().findElement(priorityFieldLocator).click();
		getDriver().findElement(priorityFieldLocator).clear();
		getDriver().findElement(priorityFieldLocator).sendKeys(priority);
		getDriver().findElement(By.cssSelector("button.aui-button.submit")).click();

		return this;
	}

	/**
	 * Returns a String which specifies the issue priority. 
	 * There are 5 issue priorities: Trivial, Minor, Major, Critical, Blocker. 
	 * <p>
	 * This method to get the issue priority. 
	 *
	 * @param  
	 * @return String
	 */
	public String getPriority() {
		Reporter.log("Get the issue priority </br>", true);
		return getDriver().findElement(By.xpath("//span[@id='priority-val']")).getText();
	}
	
	/**
	 * Returns true if the priority is edited correctly. 
	 * The priority can be defined in ExerciseConstants.java.
	 * There are 5 issue priorities: Trivial, Minor, Major, Critical, Blocker. 
	 * <p>
	 * This method to verify that the priority is edited correctly. 
	 * The method writes the PASSED/FAILED result into /target/surefire-reports/html
	 *
	 * @param  expectedPriority  is a string which specifies the priority.
	 * @return Boolean
	 */
	public Boolean verifyPriority(String expectedPriority) {
		Reporter.log("Verify if the current priority equals as expected priority </br>", true);
		if (getPriority().contains(expectedPriority))
		{
			Reporter.log("--- PASSED </br>", true);
			return true;
		}
		Reporter.log("--- FAILED </br>", true);
		return false;
	}

	/**
	 * Returns an EditIssuePage object. 
	 * The summary can be defined in ExerciseConstants.java.
	 *  
	 * <p>
	 * This method to edit the summary. 
	 *
	 * @param  summary  is a string which describes the issue summary.
	 * @return EditIssuePage
	 */
	public EditIssuePage editSummary(String summary) {
		Reporter.log("Edit the issue summary </br>", true);
		getDriver().findElement(By.xpath("//h1[@id='summary-val']")).click();
		getDriver().findElement(summaryLocator).click();
		getDriver().findElement(summaryLocator).clear();
		getDriver().findElement(summaryLocator).sendKeys(summary);
		getDriver().findElement(By.cssSelector("button.aui-button.submit")).click();

		return this;
	}

	/**
	 * Returns a String which specifies the issue summary. 
	 * <p>
	 * This method to get the issue summary. 
	 *
	 * @param  
	 * @return String
	 */
	public String getSummary() {
		Reporter.log("Get the issue summary </br>", true);
		return getDriver().findElement(By.xpath("//h1[@id='summary-val']")).getText();
	}

	/**
	 * Returns true if the summary is edited correctly. 
	 * The expectedSummary can be defined in ExerciseConstants.java.
	 * <p>
	 * This method to verify that the summary is edited correctly. 
	 * The method writes the PASSED/FAILED result into /target/surefire-reports/html
	 *
	 * @param  expectedSummary  is a string which specifies the summary.
	 * @return Boolean
	 */
	public Boolean verifySummary(String expectedSummary) {
		Reporter.log("Verify if the current summary equals as expected summary </br>", true);
		if (getSummary().contains(expectedSummary))
		{
			Reporter.log("--- PASSED </br>", true);
			return true;
		}
		Reporter.log("--- FAILED </br>", true);
		return false;
	}

	/**
	 * Returns true if the type, priority and summary is edited correctly. 
	 * The issueType, priority and summary can be defined in ExerciseConstants.java.
	 * <p>
	 * This method to verify that the type, priority and summary is edited correctly. 
	 * The method writes the PASSED/FAILED result into /target/surefire-reports/html
	 *
	 * @param  issueType  is a string which describes the issue issue type.
	 * @param  priority  is a string which describes the issue priority.
	 * @param  summary  is a string which describes the issue summary.
	 * @return Boolean
	 */
	public Boolean verifyEditExistingIssue(String issueType, String priority, String summary) {
		Reporter.log("Verify if an existing issue can be edited successfully </br>", true);
		editIssueType(issueType);
		waitSeconds(3000);
		if (verifyIssueType(issueType) == false)
		{
			return false;
		}	
		
		editPriority(priority);
		waitSeconds(3000);
		if (verifyPriority(priority) == false)
		{
			return false;
		}
		
		editSummary(summary);
		waitSeconds(5000);
		if (verifySummary(summary) == false)
		{
			return false;
		}
		
		return true;
	}
	
	/**
	 * Returns an EditIssuePage object.
	 *  
	 * <p>
	 * This method to click on submit button. The issue will be updated. 
	 *
	 * @param
	 * @return EditIssuePage
	 */
	public EditIssuePage submitEditIssue() {

		Reporter.log("Submit issue </br>", true);
		getDriver().findElement(issueUpdateButtonLocator).click();
		
		return this;
	}

	@Override
	public EditIssuePage getPage() {
		return this;
	}

}