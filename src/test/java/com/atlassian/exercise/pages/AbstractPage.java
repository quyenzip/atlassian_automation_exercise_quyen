package com.atlassian.exercise.pages;

import org.openqa.selenium.WebDriver;

public abstract class AbstractPage<T> {

	private final WebDriver driver;

	public AbstractPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public abstract T getPage();

	public T waitSeconds(long delayTime) {
		try {
			Thread.sleep(delayTime);
		} catch (InterruptedException e) {
			//Do nothings
		}
		
		return getPage();
	}
}
