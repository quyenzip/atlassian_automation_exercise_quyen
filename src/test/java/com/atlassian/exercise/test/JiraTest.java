package com.atlassian.exercise.test;

import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.atlassian.exercise.pages.CreateIssuePage;
import com.atlassian.exercise.pages.HomePage;
import com.atlassian.exercise.util.ExerciseConstants;
import com.atlassian.exercise.util.ExerciseUtils;

public class JiraTest {
	private WebDriver driver;
	private StringBuffer verificationErrors = new StringBuffer();
	private Properties properties;

	@BeforeTest
	@Parameters({ "config-file" })
	public void initFramework(@Optional("src/test/resources/config.properties/") String configfile) throws IOException {
		Reporter.log("Invoked init Method <br />", true);

		properties = new Properties();
		FileInputStream conf = new FileInputStream(configfile);
		properties.load(conf);

		String timeOutString = properties.getProperty(ExerciseConstants.TIME_OUT_KEY, String.valueOf(30));
		String driverClass = properties.getProperty(ExerciseConstants.BROWSER_DRIVER_CLASS_KEY, FirefoxDriver.class.getName());

		driver = (WebDriver) ExerciseUtils.createObject(driverClass);
		driver.manage().timeouts().implicitlyWait(Integer.valueOf(timeOutString).intValue(), TimeUnit.SECONDS);
	}

	@Test
	/**
	 * This is TEST-1 to verify that a new issue can be created successfully on JIRA 
	 * The method does not return.
	 *  
	 * <p>
	 * This method does:
	 * - Navigate to homepage
	 * - Login to JIRA account
	 * - Create a new issue with: summary, issue type, environment, priority, description
	 * - Submit the issue
	 * - Verify a success message is displayed
	 * 
	 * @param  
	 * @return void
	 */
	public void testCreateIssue() throws Exception {
		// Start test
		Reporter.log("Start test</br>", true);

		HomePage jiraPage = new HomePage(driver);

		CreateIssuePage createIssuePage = jiraPage.loadPage(ExerciseConstants.BROWSER_URL).waitSeconds(5000)
		// Precondition: Login to JIRA account
				.gotoLogin().loginAs(ExerciseConstants.USERNAME, ExerciseConstants.PASSWORD, false).waitSeconds(1000)

				// TEST 1: Verify that a new bug can be created on JIRA
				.gotoCreateNewIssue().waitSeconds(1000)
				// Verify that the issue is created successfully and enter the
				.createIssue(ExerciseConstants.ISSUE_TYPE_BUG, ExerciseConstants.ISSUE_SUMMARY_TEXT, ExerciseConstants.PRIORITY_MAJOR, ExerciseConstants.ENVIRONMENT_TEXT, ExerciseConstants.DESCRIPTION_TEXT);
				//.waitSeconds(10000);

		Assert.assertTrue(createIssuePage.isMessageIssueCreatedSuccessfullyDisplayed());

	}

	@Test
	/**
	 * This is TEST-2 to verify that an existing issue can be edited successfully on JIRA 
	 * The method does not return.
	 *  
	 * <p>
	 * This method does:
	 * - Navigate to issue page
	 * - Login to JIRA account
	 * - Edit the issue info: summary, issue type, priority...
	 * - Submit the edited issue
	 * - Verify the edited info is correct
	 * 
	 * @param  
	 * @return void
	 */
	public void testEditIssue() throws Exception {
		// Start test
		Reporter.log("Start test</br>", true);

		HomePage jiraPage = new HomePage(driver);

		jiraPage.loadPage(ExerciseConstants.EDIT_ISSUE_BROWSER_URL).waitSeconds(5000)
		// Precondition: Login to JIRA account
				.gotoLogin().loginAs(ExerciseConstants.USERNAME, ExerciseConstants.PASSWORD, false).waitSeconds(1000)

				// TEST 2: Verify that an existing issue can be edited on JIRA
				.gotoEditIssue()
				// Verify that the issue type can be edited correctly
				.verifyEditExistingIssue(ExerciseConstants.ISSUE_TYPE_NEWFEATURE, ExerciseConstants.PRIORITY_MAJOR, ExerciseConstants.ISSUE_SUMMARY_TEXT_EDITED);
	}

	@Test
	/**
	 * This is TEST-3 to verify that an existing issue can be searched successfully on JIRA search
	 * The method does not return.
	 *  
	 * <p>
	 * This method does:
	 * - Navigate to home page
	 * - Login to JIRA account
	 * - Submit an issue ID into JIRA search field
	 * - Verify the existing issue can be found
	 * 
	 * @param  
	 * @return void
	 */
	public void testSearchIssue() throws Exception {
		// Start test
		Reporter.log("Start test</br>", true);

		HomePage jiraPage = new HomePage(driver);

		jiraPage.loadPage(ExerciseConstants.BROWSER_URL).waitSeconds(5000)
		// Precondition: Login to JIRA account
				.gotoLogin().loginAs(ExerciseConstants.USERNAME, ExerciseConstants.PASSWORD, false).waitSeconds(1000)

				// TEST 3: Verify that an existing issue can be searched on JIRA
				.gotoSearchIssue().waitSeconds(1000)

				// Search an existing issue
				.searchExistingIssue(ExerciseConstants.ISSUE_ID_TEST).waitSeconds(5000)
				// Verify that the issue is found successfully
				.verifySearchResult(ExerciseConstants.ISSUE_ID_TEST);

	}

	@AfterTest
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}
}
